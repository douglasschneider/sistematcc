package sistematcc;

import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import entidades.Aluno;
import entidades.Banca;
import entidades.Professor;
import entidades.Trabalho;

import javax.swing.JButton;

import java.awt.Component;
import javax.swing.Box;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;

public class MainGUI {

	private JFrame frame;
	private JTable tblProfessor;
	private JTextField txtNomeProfessor;
	private JTextField txtEmailProfessor;
	private JTextField txtTelefoneProfessor;
	private JTextField txtAnoIngressoProfessor;
	private JTextField txtTitulacaoProfessor;
	private JTable tblAluno;
	private JTextField txtNomeAluno;
	private JTextField txtEmailAluno;
	private JTextField txtTelefoneAluno;
	private JTextField txtAnoIngressoAluno;
	private JTextField txtAnoConclusaoAluno;
	private JTable tblTrabalho;
	private JTable tblBanca;
	private JTextField txtTituloTrabalho;

	// Vetores do Programa
	ArrayList<Professor> professores;
	ArrayList<Aluno> alunos;
	ArrayList<Trabalho> trabalhos;
	ArrayList<Banca> bancaTrabalhos;
	
	// Variável utilizada para edição
	String modo;
	private JTextField txtNotaBanca;
	
	private JComboBox cbAlunoTrabalho;
	private JComboBox cbProfessorTrabalho;
	private JComboBox cbTrabalhoBanca;
	private JComboBox cbProfessorBanca;
	
	// Carregar a lista de alunos para o ComboBox
	public void carregaAlunos() {
		cbAlunoTrabalho.removeAllItems();
		for (int i = 0; i < alunos.size(); i++) cbAlunoTrabalho.addItem(alunos.get(i).getNome());
	}
	
	// Carregar a lista de professores para o ComboBox
	public void carregaProfessores() {
		cbProfessorTrabalho.removeAllItems();
		for (int i = 0; i < professores.size(); i++) cbProfessorTrabalho.addItem(professores.get(i).getNome());
	}
	
	// Carregar a lista de trabalhos para o ComboBox
	public void carregaTrabalhos() {
		cbTrabalhoBanca.removeAllItems();
		for (int i = 0; i < trabalhos.size(); i++) cbTrabalhoBanca.addItem(trabalhos.get(i).getTitulo());
	}
	
	// Carregar a lista de professores da banca para o ComboBox
	public void carregaProfessorBanca() {
		cbProfessorBanca.removeAllItems();
		for (int i = 0; i < professores.size(); i++) cbProfessorBanca.addItem(professores.get(i).getNome());
		cbProfessorBanca.addItem("Auto Avaliação");
	}
	
	// Carregar a lista de professores na tabela a ser exibida
	public void carregaTabelaProfessores() {
		DefaultTableModel modelo = new DefaultTableModel(new Object[] { "Nome", "E-mail", "Telefone", "Ano de Ingresso", "Titulação" }, 0);
		tblProfessor.setModel(modelo);
		for (int i = 0; i < professores.size(); i++) {
			modelo.addRow(new Object[] { 
				professores.get(i).getNome(),
				professores.get(i).getEmail(),
				professores.get(i).getTelefone(),
				professores.get(i).getAnoIngresso(),
				professores.get(i).getTitulacao()
			});
		}
	}
	
	// Carregar a lista de alunos na tabela
	public void carregaTabelaAlunos() {
		DefaultTableModel modelo = new DefaultTableModel(new Object[] { "Nome", "E-mail", "Telefone", "Ano de Ingresso", "Titulação" }, 0);
		tblAluno.setModel(modelo);
		for (int i = 0; i < alunos.size(); i++) {
			modelo.addRow(new Object[] {
					alunos.get(i).getNome(),
					alunos.get(i).getEmail(),
					alunos.get(i).getTelefone(),
					alunos.get(i).getAnoIngresso(),
					alunos.get(i).getAnoConclusao()
			});
		}
	}
	
	// Carregar a lista de trabalhos
	public void carregaTabelaTrabalhos() {
		DefaultTableModel modelo = new DefaultTableModel(new Object[] { "Título", "Aluno", "Professor", "Nota Final" }, 0);
		tblTrabalho.setModel(modelo);
		for (int i = 0; i < trabalhos.size(); i++) {
			modelo.addRow(new Object[] {
					trabalhos.get(i).getTitulo(),
					trabalhos.get(i).getAluno().getNome(),
					trabalhos.get(i).getProfessor().getNome(),
					trabalhos.get(i).getNotaFinal()
			});
		}
	}
	
	// Carregar a lista da banca
	public void carregaTabelaBanca() {
		DefaultTableModel modelo = new DefaultTableModel(new Object[] { "Trabalho", "Data da Banca", "Professor", "Nota" }, 0);
		tblBanca.setModel(modelo);
		for (int i = 0; i < bancaTrabalhos.size(); i++) {
			modelo.addRow(new Object[] {
					bancaTrabalhos.get(i).getTrabalho(),
					bancaTrabalhos.get(i).getProfessores().get(i),
					bancaTrabalhos.get(i).getNotas().get(i)
			});		
		}
	}
	
	/**
	 * Launch the application.
	 */
	
	/* 
	 * @param for args
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainGUI() {
		initialize();
		professores = new ArrayList<Professor>();
		alunos = new ArrayList<Aluno>();
		trabalhos = new ArrayList<Trabalho>();
		bancaTrabalhos = new ArrayList<Banca>();
	}

	
	private void initialize() {
		// Professor
		frame = new JFrame();
		frame.setBounds(100, 100, 569, 423);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 553, 384);
		frame.getContentPane().add(tabbedPane);
		
		JPanel professor = new JPanel();
		tabbedPane.addTab("Professor", null, professor, null);
		professor.setLayout(null);
		
		JScrollPane spProfessor = new JScrollPane();
		spProfessor.setBounds(10, 5, 528, 126);
		professor.add(spProfessor);
		
		tblProfessor = new JTable();
		tblProfessor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = tblProfessor.getSelectedRow();
				if(index >= 0 && index < professores.size()) {
					Professor prf = professores.get(index);
					txtNomeProfessor.setText(prf.getNome());
					txtEmailProfessor.setText(prf.getEmail());
					txtTelefoneProfessor.setText(prf.getTelefone());
					txtAnoIngressoProfessor.setText(Integer.toString(prf.getAnoIngresso()));
					txtTitulacaoProfessor.setText(prf.getTitulacao());
				}
			}
		});
		tblProfessor.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"Nome", "E-mail", "Telefone", "Ano de Ingresso", "Titula\u00E7\u00E3o"
			}
		));
		spProfessor.setViewportView(tblProfessor);
		
		JTextPane txtpnNomeProfessor = new JTextPane();
		txtpnNomeProfessor.setText("Nome");
		txtpnNomeProfessor.setBounds(10, 198, 40, 20);
		professor.add(txtpnNomeProfessor);
		
		JTextPane txtpnEmailProfessor = new JTextPane();
		txtpnEmailProfessor.setText("E-mail");
		txtpnEmailProfessor.setBounds(10, 229, 50, 20);
		professor.add(txtpnEmailProfessor);
		
		JTextPane txtpnTelefoneProfessor = new JTextPane();
		txtpnTelefoneProfessor.setText("Telefone");
		txtpnTelefoneProfessor.setBounds(10, 261, 58, 20);
		professor.add(txtpnTelefoneProfessor);
		
		JTextPane txtpnAnoDeIngressoProfessor = new JTextPane();
		txtpnAnoDeIngressoProfessor.setText("Ano de Ingresso");
		txtpnAnoDeIngressoProfessor.setBounds(10, 292, 109, 20);
		professor.add(txtpnAnoDeIngressoProfessor);
		
		JTextPane txtpnTitulaoProfessor = new JTextPane();
		txtpnTitulaoProfessor.setText("Titula\u00E7\u00E3o");
		txtpnTitulaoProfessor.setBounds(10, 323, 69, 20);
		professor.add(txtpnTitulaoProfessor);
		
		txtNomeProfessor = new JTextField();
		txtNomeProfessor.setBounds(60, 198, 279, 20);
		professor.add(txtNomeProfessor);
		txtNomeProfessor.setColumns(10);
		
		txtEmailProfessor = new JTextField();
		txtEmailProfessor.setBounds(72, 229, 279, 20);
		professor.add(txtEmailProfessor);
		txtEmailProfessor.setColumns(10);
		
		txtTelefoneProfessor = new JTextField();
		txtTelefoneProfessor.setBounds(80, 261, 109, 20);
		professor.add(txtTelefoneProfessor);
		txtTelefoneProfessor.setColumns(10);
		
		txtAnoIngressoProfessor = new JTextField();
		txtAnoIngressoProfessor.setBounds(131, 292, 69, 20);
		professor.add(txtAnoIngressoProfessor);
		txtAnoIngressoProfessor.setColumns(10);
		
		txtTitulacaoProfessor = new JTextField();
		txtTitulacaoProfessor.setBounds(91, 323, 258, 20);
		professor.add(txtTitulacaoProfessor);
		txtTitulacaoProfessor.setColumns(10);
		
		JButton btnCadastrarProfessor = new JButton("Cadastrar");
		btnCadastrarProfessor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(modo == "Novo") {
					Professor pfr = new Professor(txtNomeProfessor.getText(),
							txtEmailProfessor.getText(),
							txtTelefoneProfessor.getText(),
							Integer.parseInt(txtAnoIngressoProfessor.getText()),
							txtTitulacaoProfessor.getText());
					professores.add(pfr);
				}
				else if(modo == "Editar") {
					int index = tblProfessor.getSelectedRow();
					professores.get(index).setNome(txtNomeProfessor.getText());
					professores.get(index).setEmail(txtEmailProfessor.getText());
					professores.get(index).setTelefone(txtTelefoneProfessor.getText());
					professores.get(index).setAnoIngresso(Integer.parseInt(txtAnoIngressoProfessor.getText()));
					professores.get(index).setTitulacao(txtTitulacaoProfessor.getText());
				}
				btnCadastrarProfessor.setEnabled(false);
				txtNomeProfessor.setEnabled(false);
				txtEmailProfessor.setEnabled(false);
				txtTelefoneProfessor.setEnabled(false);
				txtAnoIngressoProfessor.setEnabled(false);
				txtTitulacaoProfessor.setEnabled(false);
				carregaTabelaProfessores();
				txtNomeProfessor.setText("");
				txtEmailProfessor.setText("");
				txtTelefoneProfessor.setText("");
				txtAnoIngressoProfessor.setText("");
				txtTitulacaoProfessor.setText("");
			}
		});
		btnCadastrarProfessor.setBounds(412, 245, 126, 23);
		professor.add(btnCadastrarProfessor);
		btnCadastrarProfessor.setEnabled(false);
		
		JButton btnNovoProfessor = new JButton("Novo");
		btnNovoProfessor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Novo";
				btnCadastrarProfessor.setEnabled(true);
				txtNomeProfessor.setEnabled(true);
				txtEmailProfessor.setEnabled(true);
				txtTelefoneProfessor.setEnabled(true);
				txtAnoIngressoProfessor.setEnabled(true);
				txtTitulacaoProfessor.setEnabled(true);
				
			}
		});
		btnNovoProfessor.setBounds(10, 142, 89, 23);
		professor.add(btnNovoProfessor);
		
		JButton btnEditarProfessor = new JButton("Editar");
		btnEditarProfessor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Editar";
				btnCadastrarProfessor.setEnabled(true);
				txtNomeProfessor.setEnabled(true);
				txtEmailProfessor.setEnabled(true);
				txtTelefoneProfessor.setEnabled(true);
				txtAnoIngressoProfessor.setEnabled(true);
				txtTitulacaoProfessor.setEnabled(true);
			}
		});
		btnEditarProfessor.setBounds(233, 142, 89, 23);
		professor.add(btnEditarProfessor);
		
		JButton btnExcluirProfessor = new JButton("Excluir");
		btnExcluirProfessor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Excluir";
				int index = tblProfessor.getSelectedRow();
				if(index >= 0 && index < professores.size()) {
					professores.remove(index);
					carregaTabelaProfessores();
				}
			}
		});
		btnExcluirProfessor.setBounds(449, 142, 89, 23);
		professor.add(btnExcluirProfessor);
		
		// Aluno
		JPanel aluno = new JPanel();
		tabbedPane.addTab("Aluno", null, aluno, null);
		aluno.setLayout(null);
		
		JScrollPane spAluno = new JScrollPane();
		spAluno.setBounds(10, 5, 528, 126);
		aluno.add(spAluno);
		
		tblAluno = new JTable();
		tblAluno.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = tblAluno.getSelectedRow();
				if(index >= 0 && index < alunos.size()) {
					Aluno aln = alunos.get(index);
					txtNomeAluno.setText(aln.getNome());
					txtEmailAluno.setText(aln.getEmail());
					txtTelefoneAluno.setText(aln.getTelefone());
					txtAnoIngressoAluno.setText(Integer.toString(aln.getAnoIngresso()));
					txtAnoConclusaoAluno.setText(Integer.toString(aln.getAnoConclusao()));
				}
			}
		});
		tblAluno.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"Nome", "E-mail", "Telefone", "Ano de Ingresso", "Ano de Conclusão"
			}
		));
		spAluno.setViewportView(tblAluno);
		
		JTextPane txtpnNomeAluno = new JTextPane();
		txtpnNomeAluno.setText("Nome");
		txtpnNomeAluno.setBounds(10, 176, 41, 20);
		aluno.add(txtpnNomeAluno);
		
		JTextPane txtpnEmailAluno = new JTextPane();
		txtpnEmailAluno.setText("E-mail");
		txtpnEmailAluno.setBounds(10, 207, 50, 20);
		aluno.add(txtpnEmailAluno);
		
		JTextPane txtpnTelefoneAluno = new JTextPane();
		txtpnTelefoneAluno.setText("Telefone");
		txtpnTelefoneAluno.setBounds(10, 238, 57, 20);
		aluno.add(txtpnTelefoneAluno);
		
		JTextPane txtpnAnoDeIngressoAluno = new JTextPane();
		txtpnAnoDeIngressoAluno.setText("Ano de Ingresso");
		txtpnAnoDeIngressoAluno.setBounds(10, 266, 107, 20);
		aluno.add(txtpnAnoDeIngressoAluno);
		
		JTextPane txtpnAnoDeConclusaoAluno = new JTextPane();
		txtpnAnoDeConclusaoAluno.setText("Ano de Conclus\u00E3o");
		txtpnAnoDeConclusaoAluno.setBounds(10, 296, 114, 20);
		aluno.add(txtpnAnoDeConclusaoAluno);
		
		txtNomeAluno = new JTextField();
		txtNomeAluno.setBounds(61, 176, 246, 20);
		aluno.add(txtNomeAluno);
		txtNomeAluno.setColumns(10);
		
		txtEmailAluno = new JTextField();
		txtEmailAluno.setBounds(71, 207, 246, 20);
		aluno.add(txtEmailAluno);
		txtEmailAluno.setColumns(10);
		
		txtTelefoneAluno = new JTextField();
		txtTelefoneAluno.setBounds(79, 238, 140, 20);
		aluno.add(txtTelefoneAluno);
		txtTelefoneAluno.setColumns(10);
		
		txtAnoIngressoAluno = new JTextField();
		txtAnoIngressoAluno.setBounds(129, 266, 101, 20);
		aluno.add(txtAnoIngressoAluno);
		txtAnoIngressoAluno.setColumns(10);
		
		txtAnoConclusaoAluno = new JTextField();
		txtAnoConclusaoAluno.setBounds(139, 296, 96, 20);
		aluno.add(txtAnoConclusaoAluno);
		txtAnoConclusaoAluno.setColumns(10);
		
		JButton btnCadastrarAluno = new JButton("Cadastrar");
		btnCadastrarAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(modo == "Novo") {
					Aluno aln = new Aluno(txtNomeAluno.getText(),
							txtEmailAluno.getText(),
							txtTelefoneAluno.getText(),
							Integer.parseInt(txtAnoIngressoAluno.getText()),
							Integer.parseInt(txtAnoConclusaoAluno.getText()));
					alunos.add(aln);
				}
				else if(modo == "Editar") {
					int index = tblAluno.getSelectedRow();
					alunos.get(index).setNome(txtNomeAluno.getText());
					alunos.get(index).setEmail(txtEmailAluno.getText());
					alunos.get(index).setTelefone(txtTelefoneAluno.getText());
					alunos.get(index).setAnoIngresso(Integer.parseInt(txtAnoIngressoAluno.getText()));
					alunos.get(index).setAnoConclusão(Integer.parseInt(txtAnoConclusaoAluno.getText()));
				}
				txtNomeAluno.setText("");
				txtEmailAluno.setText("");
				txtTelefoneAluno.setText("");
				txtAnoIngressoAluno.setText("");
				txtAnoConclusaoAluno.setText("");
				btnCadastrarAluno.setEnabled(false);
				txtNomeAluno.setEnabled(false);
				txtEmailAluno.setEnabled(false);
				txtTelefoneAluno.setEnabled(false);
				txtAnoIngressoAluno.setEnabled(false);
				txtAnoConclusaoAluno.setEnabled(false);
				carregaTabelaAlunos();
			}
		});
		btnCadastrarAluno.setBounds(373, 222, 138, 23);
		aluno.add(btnCadastrarAluno);
		
		JButton btnNovoAluno = new JButton("Novo");
		btnNovoAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Novo";
				btnCadastrarAluno.setEnabled(true);
				txtNomeAluno.setEnabled(true);
				txtEmailAluno.setEnabled(true);
				txtTelefoneAluno.setEnabled(true);
				txtAnoIngressoAluno.setEnabled(true);
				txtAnoConclusaoAluno.setEnabled(true);
			}
		});
		btnNovoAluno.setBounds(10, 142, 89, 23);
		aluno.add(btnNovoAluno);
		
		JButton btnEditarAluno = new JButton("Editar");
		btnEditarAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Editar";
				btnCadastrarAluno.setEnabled(true);
				txtNomeAluno.setEnabled(true);
				txtEmailAluno.setEnabled(true);
				txtTelefoneAluno.setEnabled(true);
				txtAnoIngressoAluno.setEnabled(true);
				txtAnoConclusaoAluno.setEnabled(true);
			}
		});
		btnEditarAluno.setBounds(232, 142, 89, 23);
		aluno.add(btnEditarAluno);
		
		JButton btnExcluirAluno = new JButton("Excluir");
		btnExcluirAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				modo = "Excluir";
				int index = tblAluno.getSelectedRow();
				if(index >= 0 && index < alunos.size()) {
					alunos.remove(index);
					carregaTabelaAlunos();
				}
			}
		});
		btnExcluirAluno.setBounds(449, 142, 89, 23);
		aluno.add(btnExcluirAluno);
		
		// Trabalho
		JPanel trabalho = new JPanel();
		tabbedPane.addTab("Trabalho", null, trabalho, null);
		trabalho.setLayout(null);
		
		JScrollPane spTrabalho = new JScrollPane();
		spTrabalho.setBounds(10, 5, 528, 126);
		trabalho.add(spTrabalho);
		
		tblTrabalho = new JTable();
		tblTrabalho.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = tblTrabalho.getSelectedRow();
				if(index >= 0 && index < trabalhos.size()) {
					Trabalho tba = trabalhos.get(index);
					txtTituloTrabalho.setText(tba.getTitulo());
					cbAlunoTrabalho.addItem(alunos.get(index).getNome());
					cbProfessorTrabalho.addItem(alunos.get(index).getNome());
				}
			}
		});
		tblTrabalho.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"T\u00EDtulo", "Aluno", "Professor", "Nota Final"
			}
		));
		spTrabalho.setViewportView(tblTrabalho);
		
		JTextPane txtpnTituloTrabalho = new JTextPane();
		txtpnTituloTrabalho.setBounds(10, 193, 41, 20);
		txtpnTituloTrabalho.setText("T\u00EDtulo");
		trabalho.add(txtpnTituloTrabalho);
		
		txtTituloTrabalho = new JTextField();
		txtTituloTrabalho.setBounds(61, 193, 238, 20);
		trabalho.add(txtTituloTrabalho);
		txtTituloTrabalho.setColumns(10);
		
		JTextPane txtpnAlunoTrabalho = new JTextPane();
		txtpnAlunoTrabalho.setBounds(10, 224, 41, 20);
		txtpnAlunoTrabalho.setText("Aluno");
		trabalho.add(txtpnAlunoTrabalho);
		
		JTextPane txtpnProfessorTrabalho = new JTextPane();
		txtpnProfessorTrabalho.setBounds(10, 252, 64, 20);
		txtpnProfessorTrabalho.setText("Professor");
		trabalho.add(txtpnProfessorTrabalho);
		
		cbAlunoTrabalho = new JComboBox();
		cbAlunoTrabalho.setBounds(61, 220, 238, 24);
		trabalho.add(cbAlunoTrabalho);
		
		cbProfessorTrabalho = new JComboBox();
		cbProfessorTrabalho.setBounds(86, 252, 213, 24);
		trabalho.add(cbProfessorTrabalho);
		
		JButton btnCadastrarTrabalho = new JButton("Cadastrar");
		btnCadastrarTrabalho.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (modo == "Novo") {
					Trabalho tba = new Trabalho();
					tba.setTitulo(txtTituloTrabalho.getText());
					int index = cbAlunoTrabalho.getSelectedIndex();
					int i = cbProfessorTrabalho.getSelectedIndex();
					tba.setAluno(alunos.get(index));
					tba.setProfessor(professores.get(i));
					trabalhos.add(tba);
				}
				else if(modo == "Editar") {
					int index = tblTrabalho.getSelectedRow();
					trabalhos.get(index).setTitulo(txtTituloTrabalho.getText());
				}
				btnCadastrarTrabalho.setEnabled(false);
				txtTituloTrabalho.setEnabled(false);
				cbAlunoTrabalho.setEnabled(false);
				cbProfessorTrabalho.setEnabled(false);
				txtTituloTrabalho.setText("");
				carregaTabelaTrabalhos();
			}
		});
		btnCadastrarTrabalho.setBounds(215, 300, 130, 23);
		trabalho.add(btnCadastrarTrabalho);
		
		JButton btnNovoTrabalho = new JButton("Novo");
		btnNovoTrabalho.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				carregaProfessores();
				carregaAlunos();
				modo = "Novo";
				btnCadastrarTrabalho.setEnabled(true);
				txtTituloTrabalho.setEnabled(true);
				cbAlunoTrabalho.setEnabled(true);
				cbProfessorTrabalho.setEnabled(true);
				txtTituloTrabalho.setText("");
			}
		});
		btnNovoTrabalho.setBounds(163, 153, 89, 23);
		trabalho.add(btnNovoTrabalho);
		
		JButton btnEditarTrabalho = new JButton("Editar");
		btnEditarAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				carregaAlunos();
				carregaProfessores();
				modo = "Editar";
				btnCadastrarTrabalho.setEnabled(true);
				txtTituloTrabalho.setEnabled(true);
				cbAlunoTrabalho.setEnabled(true);
				cbProfessorTrabalho.setEnabled(true);
			}
		});
		btnEditarTrabalho.setBounds(300, 153, 89, 23);
		trabalho.add(btnEditarTrabalho);
		
		// Banca
		JPanel banca = new JPanel();
		tabbedPane.addTab("Banca", null, banca, null);
		banca.setLayout(null);
		
		JScrollPane spBanca = new JScrollPane();
		spBanca.setBounds(10, 5, 528, 126);
		banca.add(spBanca);
		
		tblBanca = new JTable();
		tblBanca.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int index = tblBanca.getSelectedRow();
				if(index >= 0 && index < bancaTrabalhos.size()) {
					Banca bca = bancaTrabalhos.get(index);
					txtNotaBanca.setText(Double.toString(bca.getNotas().get(index)));
					cbTrabalhoBanca.addItem(bancaTrabalhos.get(index).getTrabalho().getTitulo());
					cbProfessorBanca.addItem(bancaTrabalhos.get(index).getProfessores().get(index).getNome());
				}
			}
		});
		tblBanca.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"Trabalho", "Professor", "Nota"
			}
		));
		spBanca.setViewportView(tblBanca);
		
		JTextPane txtpnTrabalhoBanca = new JTextPane();
		txtpnTrabalhoBanca.setText("Trabalho");
		txtpnTrabalhoBanca.setBounds(10, 185, 60, 20);
		banca.add(txtpnTrabalhoBanca);
		
		JTextPane txtpnProfessorBanca = new JTextPane();
		txtpnProfessorBanca.setText("Professor");
		txtpnProfessorBanca.setBounds(10, 217, 60, 20);
		banca.add(txtpnProfessorBanca);
		
		JTextPane txtpnNotaBanca = new JTextPane();
		txtpnNotaBanca.setText("Nota");
		txtpnNotaBanca.setBounds(10, 249, 35, 20);
		banca.add(txtpnNotaBanca);
		
		cbTrabalhoBanca = new JComboBox();
		cbTrabalhoBanca.setBounds(83, 181, 170, 24);
		banca.add(cbTrabalhoBanca);
		
		cbProfessorBanca = new JComboBox();
		cbProfessorBanca.setBounds(83, 217, 158, 24);
		banca.add(cbProfessorBanca);
		
		txtNotaBanca = new JTextField();
		txtNotaBanca.setBounds(57, 249, 78, 19);
		banca.add(txtNotaBanca);
		txtNotaBanca.setColumns(10);
		
		JButton btnInserirBanca = new JButton("Inserir");
		btnInserirBanca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Banca bca = new Banca();
				if (modo == "Novo") {
					int index = cbAlunoTrabalho.getSelectedIndex();
					bca.setTrabalho(trabalhos.get(index));
					bca.adicionarProfessor(professores.get(index));
					bca.adicionarNotas(Double.parseDouble(txtNotaBanca.getText()));
				}
				else if (modo == "Editar") {
					int index = tblBanca.getSelectedRow();
					bancaTrabalhos.get(index).setTrabalho(trabalhos.get(index));
					bancaTrabalhos.get(index).adicionarProfessor(professores.get(index));
					bancaTrabalhos.get(index).adicionarNotas(Double.parseDouble(txtNotaBanca.getText()));
				}
				btnInserirBanca.setEnabled(false);
				cbTrabalhoBanca.setEnabled(false);
				cbProfessorBanca.setEnabled(false);
				txtNotaBanca.setEnabled(false);
				txtNotaBanca.setText("");
				carregaTabelaBanca();
				
				// Chamada de Média Final
				Trabalho tba = new Trabalho();
				try {
					tba.mediaFinal(bca.getNotas());
				} catch (Exception e2) {}
			}
		});
		btnInserirBanca.setBounds(229, 309, 89, 23);
		banca.add(btnInserirBanca);
		
		JButton btnNovoBanca = new JButton("Novo");
		btnNovoBanca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				carregaTrabalhos();
				carregaProfessorBanca();
				modo = "Novo";
				btnInserirBanca.setEnabled(true);
				cbTrabalhoBanca.setEnabled(true);
				cbProfessorBanca.setEnabled(true);
				txtNotaBanca.setEnabled(true);
				txtNotaBanca.setText("");
			}
		});
		btnNovoBanca.setBounds(164, 142, 89, 23);
		banca.add(btnNovoBanca);
		
		JButton btnEditarBanca = new JButton("Editar");
		btnEditarAluno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				carregaTrabalhos();
				carregaProfessorBanca();
				modo = "Editar";
				btnInserirBanca.setEnabled(true);
				cbTrabalhoBanca.setEnabled(true);
				cbProfessorBanca.setEnabled(true);
				txtNotaBanca.setEnabled(true);
			}
		});
		btnEditarBanca.setBounds(301, 142, 89, 23);
		banca.add(btnEditarBanca);
	}
}
