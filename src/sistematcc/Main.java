package sistematcc;

import entidades.Aluno;
import entidades.Professor;
import entidades.Trabalho;

public class Main {

	public static void main(String[] args) {
		Aluno aluno = new Aluno();
		aluno = new Aluno("Paulo", "paulo@email.com", "4990900000", 2000, 2004);
		System.out.println(aluno);
		
		Professor professor = new Professor();
		professor = new Professor("Ana", "ana@email.com", "49999909099", 2009, "Coordenadora");
		System.out.println(professor);
	}

}
