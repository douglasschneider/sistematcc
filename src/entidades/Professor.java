package entidades;

public final class Professor extends Pessoa {
	private Integer anoIngresso;
	private String titulacao;
	
	public Professor() { super(); }
	public Professor(String nome, String email, String telefone, Integer anoIngresso, String titulacao) {
		super(nome, email, telefone);
		this.anoIngresso = anoIngresso;
		this.titulacao = titulacao;
	}
	
	public Integer getAnoIngresso() {
		return anoIngresso;
	}
	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}
	
	public String getTitulacao() {
		return titulacao;
	}
	public void setTitulacao(String titulacao) {
		this.titulacao = titulacao;
	}
	
	@Override
	public String toString() {
		return super.toString() + "Ano de Ingresso: " + anoIngresso + "\nTitulação: " + titulacao + "\n";
	}
}
