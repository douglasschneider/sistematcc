package entidades;

import java.util.ArrayList;

public class Trabalho {
	private String titulo;
	private Aluno aluno;
	private Professor professor;
	private Double notaFinal;
	
	public Trabalho() {}
	public Trabalho(String titulo, Aluno aluno, Professor professor) {
		this.titulo = titulo;
		this.aluno = aluno;
		this.professor = professor;
		this.notaFinal = 0.0;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public Double getNotaFinal() {
		return notaFinal;
	}
	
	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}
	public Aluno getAluno() {
		return aluno;
	}
	
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public Professor getProfessor() {
		return professor;
	}
	
	public void mediaFinal(ArrayList<Double> notas) {
		double peso = 0.0;
		for (int i = 0; i < notas.size(); i++) {
			if(i == 0) {
				notaFinal += notas.get(i) * 3.0;
				peso += 3.0;
			} else if(i == 1 || i == 2) {
				notaFinal += notas.get(i) * 2.5;
				peso += 2.5;
			} else {
				notaFinal += notas.get(i) * 2.0;
				peso += 2.0;
			}
		}
		notaFinal /= peso;
	}
	
	@Override
	public String toString() {
		return "Ttítulo: " + titulo + "\nAluno: " + aluno + "\nProfessor: " + professor + "\nNotaFinal: " + notaFinal + "\n";
	}
}
