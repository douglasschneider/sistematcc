package entidades;

import java.util.ArrayList;

public class Banca {
	private Trabalho trabalho;
	private ArrayList<Double> notas;
	private ArrayList<Professor> professores;
	
	public Banca() {
		notas = new ArrayList<Double>(4);
		professores = new ArrayList<Professor>(3);
	}
	public Banca(Trabalho trabalho) {
		notas = new ArrayList<Double>(4);
		professores = new ArrayList<Professor>(3);
		this.trabalho = trabalho;
	}
	
	public void adicionarNotas(double nota) {
		if (nota >= 0.0 && nota <= 10.0) notas.add(nota);
	}

	public void adicionarProfessor(Professor professor) {
		professores.add(professor);
	}
	
	public Trabalho getTrabalho() {
		return trabalho;
	}
	public void setTrabalho(Trabalho trabalho) {
		this.trabalho = trabalho;
	}
	
	public ArrayList<Double> getNotas() {
		return notas;
	}
	
	public ArrayList<Professor> getProfessores() {
		return professores;
	}
}
