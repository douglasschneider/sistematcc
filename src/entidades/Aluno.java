package entidades;

public final class Aluno extends Pessoa {
	private Integer anoIngresso;
	private Integer anoConclusao;
	
	public Aluno() { super(); }
	public Aluno(String nome, String email, String telefone, Integer anoIngresso, Integer anoConclusao) {
		super(nome, email, telefone);
		this.anoIngresso = anoIngresso;
		this.anoConclusao = anoConclusao;
	}
	
	public Integer getAnoIngresso() {
		return anoIngresso;
	}
	public void setAnoIngresso(Integer anoIngresso) {
		this.anoIngresso = anoIngresso;
	}
	
	public Integer getAnoConclusao() {
		return anoConclusao;
	}
	public void setAnoConclusão(Integer anoConclusao) {
		this.anoConclusao = anoConclusao;
	}
	
	@Override
	public String toString() {
		return super.toString() + "Ano de Ingresso: " + anoIngresso + "\nAno de Conclusão: " + anoConclusao + "\n";
	}
}
